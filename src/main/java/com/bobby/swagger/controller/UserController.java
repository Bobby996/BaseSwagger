package com.bobby.swagger.controller;

import com.bobby.swagger.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javafx.geometry.Pos;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname HelloController
 * @Description TODO
 * @Date 2020/5/14 19:26
 * @Created wenjunpei
 */
@Api(tags = "用户模块的接口")
@RestController
public class UserController {


    private static List<User> userList = new ArrayList<>();

    static {
        userList.add(new User("小明","123456"));
        userList.add(new User("小红","888888"));
    }

    @ApiOperation(value = "获取用户列表",notes = "获取所有的用户列表")
    @GetMapping("/users")
    public Object findAll() {
        return userList;
    }

    @ApiOperation(value = "查找用户",notes = "通过用户的姓名查找用户")
    @GetMapping("/user/{username}")
    public User findByUserName(
            @ApiParam(value = "用户姓名",required = true)
            @PathVariable("username") String username) {
        for (User user : userList) {
            if(user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    @ApiOperation(value = "保存用户",notes = "传入用户的姓名和密码保存用户")
    @PostMapping("/user")
    public Object save(User user) {
        userList.add(user);
        return userList;
    }

    @ApiOperation(value = "删除用户",notes = "根据用户姓名删除用户")
    @DeleteMapping("/user/{username}")
    public Object delete(
            @ApiParam(value = "用户姓名",required = true)
            @PathVariable("username") String username) {
        for (User user : userList) {
            if(user.getUsername().equals(username)) {
                userList.remove(user);
            }
        }
        return userList;
    }

    @ApiOperation(value = "传统方式查找用户",notes = "根据用户姓名查找用户")
    @GetMapping("/findByUserName")
    public User findByUserNameNew(
            @ApiParam(value = "用户姓名",required = true)
            @RequestParam("username") String username
    ) {
        for (User user : userList) {
            if(user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }
}
