package com.bobby.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @Classname SwaggerConfig
 * @Description TODO
 * @Date 2020/5/14 19:32
 * @Created wenjunpei
 */

@EnableSwagger2     // 开启Swagger2的自动配置
@Configuration     // 配置类
public class SwaggerConfig {

    //配置Swagger的Docket的bean实例
    @Bean
    public Docket getDocket(Environment environment) {

        // 设置要显示的Swagger环境
        Profiles profiles = Profiles.of("dev","test");

        // 通过environment.acceptsProfiles判断是否处在自己设定的环境当中
        boolean flag = environment.acceptsProfiles(profiles);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())

                .groupName("Bobby")

                // 配置是否启动Swagger,如果为False,则Swagger不能再浏览器中访问
                .enable(flag)

                .select()
                //RequestHandlerSelectors , 配置要扫描接口的方式
                //basePackage():指定要扫描的包
                //any():扫描全部包
                //none():都不扫描包
                //withClassAnnotation() : 扫描类上的注解
                //withMethodAnnotation : 扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.bobby.swagger.controller"))
                // 配置过滤的路径
//                .paths(PathSelectors.ant("/bobby/**"))


                .build();
    }

    //配置文档信息
    private ApiInfo getApiInfo() {
        Contact contact = new Contact("联系人名字", "http://xxx.xxx.com/联系人访问链接", "联系人邮箱");
        return new ApiInfo(
                "Swagger学习", // 标题
                "学习演示如何配置Swagger", // 描述
                "v1.0", // 版本
                "http://terms.service.url/组织链接", // 组织链接
                contact, // 联系人信息
                "Apach 2.0 许可", // 许可
                "许可链接", // 许可连接
                new ArrayList<>()// 扩展
        );
    }

    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("小明");
    }

    @Bean
    public Docket docket2() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("小红");
    }

    @Bean
    public Docket docket3() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("小强");
    }
}
