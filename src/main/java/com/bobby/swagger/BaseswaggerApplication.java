package com.bobby.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseswaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseswaggerApplication.class, args);
    }

}
